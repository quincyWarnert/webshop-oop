<?php
require_once 'basic_doc.php';

class HomeDoc extends BasicDoc{
    
    public function __construct($myData){
        
        parent::__construct($myData);
    }

    protected function mainContent(){
        echo '<main><p>This the homepage</p></main>';
    }
}
?>