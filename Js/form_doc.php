<?php
 abstract class Form{

    private $_username;
    private $_email;
    private $_password;


    public function getUsername(){
        return $this->_username;
    }

    abstract public function getEmail();

    abstract public function getPassword();

 }

?>