<?php
require_once 'html_doc.php';

class BasicDoc extends HtmlDoc{
    
    protected $_data;
 
    public function __construct($myData){

        $this->_data=$myData;
    }
    protected function title(){
        echo '<title> My website-'.$this->_data['page'].'</title>';
    }

    private function metaAuthor(){ 
        echo'<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">';}
    private function cssLinks(){
        echo'<link rel="stylesheet" type="text/css" href="css/style.css">';
    }
    private function jsScript(){
        echo'
        <script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
        <script src="Js/script.js"></script>
        ';
    }
    private function bodyHeader(){
        echo 
        '<header>
        <h1>'.$this->_data['page'].'</h1>
      </header> ';  
    }
    private   function mainMenu(){
        echo'
        <nav>
             <ul>
                <li><a href="home_doc.php">Home</a></li>
                <li><a href="about_doc.php">About</a></li>
            </ul>
        </nav>
        ';
    } 

    protected function mainContent(){} 
    private   function bodyFooter(){ 
        echo '<footer>
        <p> &copy; 2019</p>
        <p> Quincy Warnert</p>

        </footer>';
    } 

        //Override function in html_doc
    
    
    
    protected function headContent(){
        $this->title();
        $this->metaAuthor();
        $this->cssLinks();
    } 

    //Override function in html_doc
    protected function bodyContent() 
    {
        $this->bodyHeader();
        $this->mainMenu();
        $this->mainContent();
        $this->bodyFooter();
    }  
}
?>

