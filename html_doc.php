<?php
class HtmlDoc{

    private function beginDoc(){
    echo'<!DOCTYPE html>
    <html lang="en" dir="ltr">';
    }

    private function beginHead(){
    echo '<head>';
    }

    protected function headContent(){
    echo'<title>First Class</title>';
    }

    private function endhead(){
    echo '</head>';
    }

    private function beginBody(){
    echo '<body>';
    }

    protected function bodyContent(){
    echo '<h1>First class content</h1>';
    }

    private function endBody(){
    echo "</body>";
    }
    private function endDoc()   {  
       echo "</html>";  
    }
    
   public function show(){
    $this->beginDoc();
    $this->beginHead();
    $this->headContent();
    $this->endhead();
    $this->beginBody();
    $this->bodyContent();
    $this->endBody();
    $this->endDoc();
    
   }
    

}
?>
